local functions = require "openfun-core/functions"
local tile_width = 32
local tile_height = 32
local grid_width = 16
local grid_height = 16
local timer_base = 1/10
local timer = 0
local record = 5
local bg = functions.generateBox(tile_width,tile_height,{30,30,30},30)
local apple = {
	img = functions.generateBox(tile_width,tile_height,{225,0,0},30),
	x = 5,
	y = 5
}
local snake = {
	img = functions.generateBox(tile_width,tile_height,{0,225,0},30),
	ax = 0,
	ay = -1,
	points = 5,
	pos = {
		{x = 10,
		y = 10
		}
	}
}
function love.update(dt)
	grid_width = math.floor(love.graphics.getWidth( )/tile_width)-2
	grid_height = math.floor(love.graphics.getHeight( )/tile_height)-1
	if snake.points > record then
		record = snake.points
	end
	if timer > 0 then
		timer = timer - dt
		return
	else
		timer = timer_base
	end
	if snake.pos[1].x < 1 then
		snake.pos[1].x = grid_width
	end
	if snake.pos[1].x > grid_width then
		snake.pos[1].x = 1
	end
	if snake.pos[1].y < 1 then
		snake.pos[1].y = grid_height
	end
	if snake.pos[1].y > grid_height then
		snake.pos[1].y = 1
	end
	local value = #snake.pos+1
	for k,v in ipairs(snake.pos) do
		for i,b in ipairs(snake.pos) do
			if (v.x == b.x and v.y == b.y 
				and i ~= k and  k ~= i-1 and k ~= i+1) then
				value = i
			end
		end
	end
	for k=#snake.pos,1,-1 do
		if k > value and k > 5 then 
			table.remove(snake.pos,k)
			snake.points = k
		else
			if k > 1 then
				if (snake.pos[k].x ~= snake.pos[k-1].x or snake.pos[k].y ~= snake.pos[k-1].y) then
				 snake.pos[k].x = snake.pos[k-1].x
				 snake.pos[k].y = snake.pos[k-1].y
				end
			else
				snake.pos[k].x = snake.pos[k].x + snake.ax
				snake.pos[k].y = snake.pos[k].y + snake.ay
			end
		end
	end
	local tx = snake.pos[#snake.pos].x
	local ty = snake.pos[#snake.pos].y
	if (snake.points > #snake.pos) then
		table.insert(snake.pos,{x = tx,y=ty})
	end
	if (snake.pos[1].x == apple.x and snake.pos[1].y == apple.y) then
		snake.points = snake.points + 1
		apple.x = math.random(grid_width)
		apple.y = math.random(grid_height)
	end
end

function love.keypressed(key,scancode)
	if snake.ax == 0 then
		if scancode == "left" then
			snake.ax = -1
			snake.ay = 0
		end
		if scancode == "right" then
			snake.ax = 1
			snake.ay = 0
		end
	else
		if scancode == "up" then
			snake.ay = -1
			snake.ax = 0
		end
		if scancode == "down" then
			snake.ay = 1
			snake.ax = 0
		end
	end
end

function love.draw()
	for x=1,grid_width do
		for y=1,grid_height do
			love.graphics.draw(bg,x*tile_width,y*tile_height)
		end
	end
	love.graphics.draw(apple.img,apple.x*tile_width,apple.y*tile_height)
	for k,v in ipairs(snake.pos) do
		love.graphics.draw(snake.img,v.x*tile_width,v.y*tile_height)
	end
	
	love.graphics.print("Points:"..snake.points.." | Record:"..record,10,10)
end
